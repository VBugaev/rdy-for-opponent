import React, { useState, Fragment } from 'react';

import { connect } from 'react-redux';

import Link from 'next/link';

import {
    Container,
    Row,
    Col,
    Navbar,
    NavbarToggler,
    NavbarBrand,
    Collapse
} from 'reactstrap';

import { getAuth } from 'src/redux/firebase.js';

import { SignInLinks, ConnectedSignOutLinks } from 'src/shared/navbarLinks';

import './_baseShell.scss';

const mapStateToProps = state => ({
    auth: getAuth(state)
});

export const BaseShell = ({ children, auth }) => {
    const [isOpen, setIsOpen] = useState(false);
    const toggle = () => setIsOpen(!isOpen);

    return (<Fragment>
        <Navbar className="app-header" color="light" light expand="md">
            <Link href="/"><NavbarBrand><h2 className="app-header-link">R 😐 F L</h2></NavbarBrand></Link>
            <NavbarToggler className="app-toggler" onClick={toggle} />
            <Collapse isOpen={isOpen} navbar>
                { auth.isEmpty ? <SignInLinks /> : <ConnectedSignOutLinks />}
            </Collapse>
        </Navbar>
        <Container fluid>
            <Row>
                <Col className="p-4">
                    {children}
                </Col>
            </Row>
        </Container>
    </Fragment>);
};

export default connect(mapStateToProps)(BaseShell);
import React, { Component, Fragment } from 'react';

import { connect } from 'react-redux';
import { fetchAuthReady, getAuthIsReady } from 'src/redux/auth.js';

import HashLoader from 'react-spinners/HashLoader';
import 'src/shells/_authReadyShell.scss';

const mapStateToProps = state => ({
    authIsReady: getAuthIsReady(state)
});
const mapDispatchToProps = dispatch => ({
    onAuthReady: () => dispatch(fetchAuthReady())
});

class AuthReadyShell extends Component {
    componentDidMount() {
        const { authIsReadyHandler, onAuthReady } = this.props;
        authIsReadyHandler.then(() => {
            onAuthReady();
        });
    }
    
    render() {
        const { authIsReady } = this.props;
        return (
            <Fragment>
                {!authIsReady ? <div className="loading-container"><HashLoader	
                    sizeUnit={"px"}
                    size={150}
                    color={'#000000'}
                    loading={true}
                /></div> : this.props.children}
            </Fragment>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(AuthReadyShell);
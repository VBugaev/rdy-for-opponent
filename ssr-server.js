const path = require('path');
const express = require('express');
const next = require('next');
const compression = require('compression');

const port = process.env.PORT || 5000;
const dev = process.env.NODE_ENV !== 'production';
const app = next({ dev });
const handle = app.getRequestHandler();

app.prepare().then(() => {
  const server = express();

  server.use(compression());

  let fbConfig = {};

  if (dev) {
    require('dotenv').config();
  }

  fbConfig = {
    apiKey: process.env.API_KEY,
    authDomain: process.env.AUTH_DOMAIN,
    databaseURL: process.env.DATABASE_URL,
    projectId: process.env.PROJECT_ID,
    storageBucket: process.env.STORAGE_BUCKET,
    messagingSenderId: process.env.MESSAGING_SENDER_ID
  }; 

  server.use('/favicon.ico', express.static(path.join(__dirname, 'public/favicon.ico')));

  server.get('/profile', (req, res) => {
    const queryParams = fbConfig;
    app.render(req, res, '/profile', queryParams);
  });

  server.get('/register', (req, res) => {
    const queryParams = fbConfig;
    app.render(req, res, '/register', queryParams);
  });

  server.get('/login', (req, res) => {
    const queryParams = fbConfig;
    app.render(req, res, '/login', queryParams);
  });

  server.get('/home', (req, res) => {
    res.redirect('/');
  });

  server.get('/', (req, res) => {
    const queryParams = fbConfig;
    app.render(req, res, '/home', queryParams);
  });


  server.get('*', (req, res) => {
    return handle(req, res);
  });

  server.listen(port, err => {
    if (err) throw err
    console.log(`> Ready on http://localhost:${port}`)
  })
});
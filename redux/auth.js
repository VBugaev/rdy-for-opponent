import { getUserByLogin } from 'src/dal/users';

export const SHOW_ERROR = 'auth/SHOW_ERROR';
export const SHOW_SUCCESS = 'auth/SHOW_SUCCESS';
export const AUTH_READY = 'auth/AUTH_READY';
export const RESET_AUTH = 'auth/RESET_AUTH';

export const showError = (code) => ({
    type: SHOW_ERROR,
    payload: {
        errorMsg: errorTypesResolver[code]
    }
});

export const fetchAuthReady = () => ({
    type: AUTH_READY
});

export const showSuccess = () => ({
    type: SHOW_SUCCESS
});

export const resetAuth = () => ({
    type: RESET_AUTH
});

const errorTypesResolver = {
    'auth/user-not-found': 'Пользователь не зарегистрирован',
    'auth/wrong-password': 'Неверный email или пароль',
    'auth/email-already-in-use': 'Email уже использован',
    'auth/login-already-exists': 'Login уже использован'
};



export const logIn = credentials => (dispatch, getState, { getFirebase }) => {
    const firebase = getFirebase();

    firebase.auth().signInWithEmailAndPassword(
        credentials.email,
        credentials.password
    ).then(() => {
        dispatch(resetAuth());
    })
        .catch(err => {
            dispatch(showError(err.code));
        })
};

export const logOut = () => (dispatch, getState, { getFirebase }) => {
    const firebase = getFirebase();

    firebase.auth().signOut();
};

export const signUp = user => (dispatch, getState, { getFirebase, getFirestore }) => {
    const firebase = getFirebase();
    const firestore = getFirestore();

    getUserByLogin(user.login, firestore).then(querySnapshot => {
        if (querySnapshot.docs.length) {
            return dispatch(showError('auth/login-already-exists'));
        } else {
            firebase.auth().createUserWithEmailAndPassword(
                user.email,
                user.password
            ).then(res => {
                return firestore.collection('users').doc(res.user.uid).set({
                    name: user.name,
                    surname: user.surname,
                    role: 'user',
                    login: user.login
                });
            }).then(() => {
                dispatch(resetAuth());
            })
                .catch(err => {
                    dispatch(showError(err.code));
                });
        }
    })
    .catch(err => console.log(err));
};

export const getErrorMsg = state => state.auth.errorMsg;
export const getIsShowSuccess = state => state.auth.showSuccess;
export const getAuthIsReady = state => state.auth.authIsReady;

export default (state = {}, action) => {
    switch (action.type) {
        case SHOW_ERROR:
            return {
                ...state,
                showSuccess: false,
                errorMsg: action.payload.errorMsg
            }
        case SHOW_SUCCESS:
            return {
                ...state,
                showSuccess: true,
                errorMsg: ''
            }
        case AUTH_READY:
            return {
                ...state,
                authIsReady: true
            }
        case RESET_AUTH:
            return {
                ...state,
                errorMsg: '',
                showSuccess: false
            }
        default:
            return state;
    }
};
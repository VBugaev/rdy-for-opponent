import { createStore, applyMiddleware } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import { combineReducers } from 'redux';
import thunkMiddleware from 'redux-thunk';

import { reducer as formReducer } from 'redux-form';

import auth from 'src/redux/auth';

import firebase from '@firebase/app';
import '@firebase/firestore';
import '@firebase/auth';

import { reactReduxFirebase, getFirebase, firebaseReducer } from 'react-redux-firebase';
import { reduxFirestore, getFirestore, firestoreReducer } from 'redux-firestore';


function getOrCreateFirebaseInstance (config) {
  if (!firebase.apps.length) {
      firebase.initializeApp(config);
    }

  return firebase;
}

export const rootReducer = combineReducers({
  auth,
  form: formReducer,
  firebase: firebaseReducer,
  firestore: firestoreReducer
});

export function initializeStore (initialState = {}, fbConfig) {
  const rrfConfig = {
    userProfile: 'users',
    useFirestoreForProfile: true,
    attachAuthIsReady: true
  };

  if (!fbConfig) {
    return createStore(rootReducer, initialState, composeWithDevTools(
      applyMiddleware(thunkMiddleware)));
  } else {
    let firebase = getOrCreateFirebaseInstance(fbConfig);
    return createStore(rootReducer, initialState, composeWithDevTools(
      reactReduxFirebase(firebase, rrfConfig),
      reduxFirestore(firebase),
      applyMiddleware(thunkMiddleware.withExtraArgument({getFirebase, getFirestore}))));
  }
 
};
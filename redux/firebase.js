export const getFirebase = state => state.firebase;
export const getAuth = state => getFirebase(state).auth;
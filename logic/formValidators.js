export const required = value => (value ? undefined : 'Обязательное поле');
export const multiRequired = value => value && value.length ? undefined : 'Необходимое поле';
export const maxLength = max => value => value && value.length > max ? `Максимум ${max} символов` : undefined;
export const minLength = min => value => value && value.length < min ? `Минимум ${min} символов` : undefined;
export const number = value => value && isNaN(Number(value)) ? 'Для ввода доступны только числа' : undefined;
export const letters = value => value && !/[a-zA-ZА-Яа-я ]+/g.test(value) ? 'Для ввода доступны только буквы' : undefined;
export const minValue = min => value => value && value < min ? `Минимум ${min} символов` : undefined;
export const email = value =>
  value && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value)
    ? 'Неверный email'
    : undefined;
import React, { Component } from 'react';

import Link from 'next/link';
import Router from 'next/router';

import { connect } from 'react-redux';

import { Nav, NavItem } from 'reactstrap';

import { logOut } from 'src/redux/auth.js';

export const SignInLinks = props => {
    return (
    <Nav className="ml-auto" navbar>
        <NavItem className="mr-md-3 mb-2 mb-md-0">
            <Link href="/login">
                <a className="btn btn-block btn-outline-success btn-lg">Войти</a>
            </Link>
        </NavItem>
        <NavItem>
            <Link href="/register">
                <a className="btn btn-block btn-outline-info btn-lg">Зарегистрироваться</a>
            </Link>
        </NavItem>
    </Nav>
    );
};

const signOutMapDispatchToProps = dispatch => ({
    onLogOut: () => dispatch(logOut())
});

const SignOutLinks = props => {
    return (
    <Nav className="ml-auto" navbar>
        <NavItem className="mr-md-3 mb-2 mb-md-0">
            <Link href="/profile">
                <a className="btn btn-block btn-outline-dark btn-lg">Профиль</a>
            </Link>
        </NavItem>
        <button onClick={props.onLogOut} className="btn btn-block btn-outline-dark btn-lg">Выйти</button>
    </Nav>
    );
};

export const ConnectedSignOutLinks = connect(null, signOutMapDispatchToProps)(SignOutLinks);
import React, { useEffect, Fragment } from 'react';

import { connect } from 'react-redux';

import Router from 'next/router';

import { getAuth } from 'src/redux/firebase.js';

const withRedirect = (redirectRoute, onLogOutFlag) => BaseComponent => {
    const component = (props) => {
        useEffect(() => {
            if (!onLogOutFlag && props.auth.uid || onLogOutFlag && !props.auth.uid) {
                Router.push(redirectRoute, redirectRoute);
            }
        });
        return (<Fragment>
                {((!onLogOutFlag && props.auth.isEmpty) || (onLogOutFlag && !props.auth.isEmpty)) && <BaseComponent {...props} />}
            </Fragment>);
    }

    const mapStateToProps = state => ({
        auth: getAuth(state)
    });

    return connect(mapStateToProps)(component);
};

export default withRedirect;
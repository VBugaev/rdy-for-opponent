const path = require('path');
const withSass = require('@zeit/next-sass');
const withCSS = require('@zeit/next-css');
const withBundleAnalyzer = require("@zeit/next-bundle-analyzer");

module.exports = withBundleAnalyzer(withCSS(withSass({
    analyzeServer: false,
    analyzeBrowser: false,
    bundleAnalyzerConfig: {
        server: {
            analyzerMode: 'static',
            reportFilename: './bundles/server.html'
        },
        browser: {
            analyzerMode: 'static',
            reportFilename: './bundles/client.html'
        }
    },
    webpack: (config, options) => {
        const appPath = __dirname;
        config.resolve.alias = {
            ...config.resolve.alias,
            src: appPath,
            srcPages: path.resolve(appPath, 'pages'),
            sharedAssets: path.resolve(appPath, 'shared'),
            pagesAssets: path.resolve(appPath, 'pagesAssets'),
            reduxModules: path.resolve(appPath, 'redux')
        };
        return config;
    }
})));
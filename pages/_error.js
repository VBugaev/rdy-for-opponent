import React from 'react'

class Error extends React.Component {
  static getInitialProps({ res, err }) {
    const statusCode = res ? res.statusCode : err ? err.statusCode : null
    return { statusCode }
  }

  render() {
    return (
      <p style={{ position: 'absolute', left: 0, right: 0,
       bottom: 0, top: 0, display: 'flex', fontSize: '6em',
        textAlign: 'center', alignItems: 'center', justifyContent: 'center', flexDirection: 'column'}}>
        <div>😔</div>
        <div>{this.props.statusCode} error</div>
      </p>
    )
  }
}

export default Error
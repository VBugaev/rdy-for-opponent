import App, {Container} from 'next/app';
import Head from 'next/head';
import React from 'react';

import withSetupShell from 'src/shells/setupShell';
import { Provider } from 'react-redux';

import AuthReadyShell from 'src/shells/authReadyShell.jsx';

import 'src/node_modules/bootstrap/dist/css/bootstrap.min.css';

class MyApp extends App {
  render () {
    const {Component, pageProps, reduxStore} = this.props;

    return (
      <Container>
        <Head>
            <meta httpEquiv="content-type" content="text/html; charset=utf-8" />
            <meta name="viewport" content="width=device-width, initial-scale=1" />
            <meta httpEquiv="X-UA-Compatible" content="ie=edge" />
            <meta name="theme-color" content="#000000" />
            <link rel="icon" href="favicon.ico" />
            <title>ROpponent</title>
        </Head>
            {reduxStore ? <Provider store={reduxStore}>
              <AuthReadyShell authIsReadyHandler={reduxStore.firebaseAuthIsReady}>
                <Component {...pageProps} />
              </AuthReadyShell>
            </Provider> : <Component {...pageProps} />}
      </Container>
    )
  }
}

export default withSetupShell(MyApp);
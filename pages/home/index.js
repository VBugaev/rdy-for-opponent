import 'srcPages/home/_index.scss';
import { firestoreConnect } from 'react-redux-firebase';

import BaseShell from 'src/shells/baseShell.jsx';

import 'src/pages/home/_index.scss';

function Home() {
    return (<BaseShell>
    <div className="home-wrapper">
      <div className="font-weight-bold">Опа, F5</div>
    </div>
    </BaseShell>);
  }
  
  export default firestoreConnect(['users'])(Home);
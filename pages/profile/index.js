import React, { Component } from 'react';
import { firestoreConnect } from 'react-redux-firebase';

import ReactTable from 'react-table';

import withRedirect from 'src/shared/withRedirectWrapper.js';

import BaseShell from 'src/shells/baseShell.jsx';

export const ProfilePage = (props) => {
    return (
        <BaseShell>Profile page</BaseShell>
    );
};

export default firestoreConnect(props => {
    return [{
        collection: 'matches',
        where: [['sport_type', '==', 'tennis']]
    }]
})(withRedirect('/', true)(ProfilePage));
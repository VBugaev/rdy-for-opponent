import React, { useEffect } from 'react';

import { connect } from 'react-redux';

import LoginForm from './components/loginForm.jsx';

import withRedirect from 'src/shared/withRedirectWrapper.js';

import { Container, Row, Col, Alert } from 'reactstrap';

import { logIn, resetAuth, getErrorMsg, getIsShowSuccess } from 'src/redux/auth.js';
import { required, maxLength, email } from 'src/logic/formValidators.js';

const mapStateToProps = state => ({
    errorMsg: getErrorMsg(state),
    showSuccess: getIsShowSuccess(state)
});

const mapDispatchToProps = dispatch => ({
    onLogin: data => dispatch(logIn(data)),
    onResetAuth: () => dispatch(resetAuth())
});

export const LoginPage = ({ onResetAuth, onLogin, errorMsg, showSuccess }) => {
    const emailValidators = [required, email, maxLength(300)];

    useEffect(() => {
        onResetAuth()
    }, []);

    return (
            <Container className="p-5">
                <Row className="d-flex justify-content-center">
                    <Col sm="8">
                        <h1 className="p-3 text-center">R 😐 F L</h1>
                        {(!showSuccess && errorMsg) && (<Alert color="danger">
                            {errorMsg}
                        </Alert>)}
                        <LoginForm emailValidators={emailValidators} onSubmit={onLogin} />
                    </Col>
                </Row>
            </Container>);
}

const withProfileRedirectPage = withRedirect('/profile')(LoginPage);

export default connect(mapStateToProps, mapDispatchToProps)(withProfileRedirectPage);
import React from 'react';
import { FormGroup, Button } from 'reactstrap';
import Link from 'next/link';
import { Field, reduxForm } from 'redux-form';
import { FormInput } from 'sharedAssets/formComponents.jsx';
import { required } from 'src/logic/formValidators.js';

const LoginForm = (props) => {
    return (
        <form onSubmit={props.handleSubmit} action="POST">
            <FormGroup>
                <Field validate={props.emailValidators} name="email" component={FormInput} type="email" placeholder="Введите email" />
            </FormGroup>
            <FormGroup>
                <Field validate={required} name="password" component={FormInput} type="password" placeholder="Введите пароль"  />
            </FormGroup>
            <Button disabled={props.submitting} outline color="primary" size="lg" block>Войти</Button>
            <Link href="/register">
                <a className="btn btn-outline-success btn-lg btn-block">Зарегистрироваться</a>
            </Link>
            <Link href="/">
                    <a className="btn btn-outline-secondary btn-lg btn-block">Вернуться на главную</a>
            </Link>
        </form>
    );
};

export default reduxForm({
    form: 'login'
})(LoginForm);
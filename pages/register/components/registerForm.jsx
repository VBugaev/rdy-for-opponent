import React from 'react';
import { FormGroup, Button } from 'reactstrap';
import Link from 'next/link';
import { Field, reduxForm, formValueSelector } from 'redux-form';
import { FormInput } from 'src/shared/formComponents';
import { connect } from 'react-redux';
import { required, maxLength, email, letters } from 'src/logic/formValidators.js';

const maxLength300 = maxLength(300);
const maxlength50 = maxLength(50);

class RegisterForm extends React.Component {
    render() {
        const { props } = this;
        return (
            <form onSubmit={props.handleSubmit} action="POST">
                <FormGroup>
                    <Field validate={[required, email, maxLength300]} name="email" component={FormInput} type="email" placeholder="Введите email" />
                </FormGroup>
                <FormGroup>
                    <Field validate={[required, maxlength50, letters]} name="login" component={FormInput} type="text" placeholder="Введите логин" />
                </FormGroup>
                <FormGroup>
                    <Field validate={[required, maxlength50, letters]} name="name" component={FormInput} type="text" placeholder="Введите имя" />
                </FormGroup>
                <FormGroup>
                    <Field validate={[required, maxlength50, letters]} name="surname" component={FormInput} type="text" placeholder="Введите фамилию" />
                </FormGroup>
                <FormGroup>
                    <Field validate={required} name="password" component={FormInput} type="password" placeholder="Введите пароль" />
                </FormGroup>
                <Button disabled={props.submitting} outline color="primary" size="lg" block>Зарегистрироваться</Button>
                <Link href="/">
                    <a className="btn btn-outline-secondary btn-lg btn-block">Вернуться на главную</a>
                </Link>
            </form>
        );
    }
}

export default reduxForm({
    form: 'register'
})(RegisterForm);
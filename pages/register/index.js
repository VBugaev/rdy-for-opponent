import React, { useEffect } from 'react';

import { connect } from 'react-redux';

import RegisterForm from './components/registerForm';

import { Container, Row, Col, Alert } from 'reactstrap';

import { signUp, resetAuth, getErrorMsg, getIsShowSuccess } from 'src/redux/auth.js';

import withRedirect from 'src/shared/withRedirectWrapper.js';

import { getAuth } from 'src/redux/firebase.js';

const mapStateToProps = state => ({
    errorMsg: getErrorMsg(state),
    showSuccess: getIsShowSuccess(state)
});

const mapDispatchToProps = dispatch => ({
    onSignUp: data => dispatch(signUp(data)),
    onResetAuth: () => dispatch(resetAuth())
});

export const RegisterPage = ({ onResetAuth, onSignUp, errorMsg, showSuccess }) => {
    useEffect(() => {
        onResetAuth()
    }, []);
    
    return (<Container className="p-5">
                <Row className="d-flex justify-content-center">
                    <Col sm="8">
                        <h1 className="p-3 text-center">R 😐 F L</h1>
                        <h1 className="text-center">Регистрация</h1>
                        {(!showSuccess && errorMsg) && (<Alert color="danger">
                            {errorMsg}
                        </Alert>)}
                        <RegisterForm onSubmit={onSignUp} />
                    </Col>
                </Row>
            </Container>);
}

const withProfileRedirectPage = withRedirect('/profile')(RegisterPage);

export default connect(mapStateToProps, mapDispatchToProps)(withProfileRedirectPage);
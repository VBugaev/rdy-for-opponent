const _LEAGUE_UPPER_BRACKET = 1;
const _LEAGUE_LOW_BRACKET = 0;

export const createLeague = (data, firestore) => {
    let matches = _createFirstMatchesForLeague(data.participants, _LEAGUE_UPPER_BRACKET, 1);

};

export const _createFirstMatchesForLeague = (participants, bracketType, tour) => {
    let matches = [];
    for (let i = 0; i < participants.length; i+=2) {
        let match = {
            user1: participants[i],
            user2: participants[i+1],
            stage: `${bracketType}${tour}${i}`,
            winner: null,
            status: null
        };

        matches.push(match);
    }
    return matches;
};
import { 
    MATCH_STATUS_PENDING,
    MATCH_STATUS_ACCEPTED,
    MATCH_STATUS_DEFINE_WINNER,
    MATCH_STATUS_FINISHED,
    MATCH_STATUS_UNKNOWN } from 'src/dal/matchConstants.js';

export const getSentMatches = (login, firestore) => {
    return firestore.collection('matches')
            .where('sender_login', '==', login).get();
};

export const getReceivedMatches = (login, firestore) => {
    return firestore.collection('matches')
            .where('receiver_login', '==', login).get();
};

export const sendMatchToUser = (data, firestore) => {
    return firestore.collection('matches')
            .add({
                sender_login: data.sender_login,
                receiver_login: data.receiver_login,
                league: '',
                sport_type: 'tennis',
                status: MATCH_STATUS_PENDING,
                sender_winner_variant: '',
                receiver_winner_variant: ''
            });
};

export const acceptMatch = (matchid, firestore) => {
    return firestore.collection('matches').doc(matchid)
            .update({
                status: MATCH_STATUS_ACCEPTED
            });
};

export const setWinnerVariant = (setterLogin, resultLogin, matchid, firestore) => {
    return firestore.collection('matches').doc(matchid).get()
            .then(doc => {
                const docData = doc.data();
                const objForUpd = {};
                
                if (docData.sender_login === setterLogin) {
                    objForUpd.sender_winner_variant = resultLogin;

                    if (docData.receiver_winner_variant) {
                        return firestore.collection('matches').doc(matchid)
                                .update({
                                    ...objForUpd,
                                    status: docData.receiver_winner_variant === objForUpd.sender_winner_variant ? MATCH_STATUS_FINISHED : MATCH_STATUS_UNKNOWN
                                })
                    }
                } else if (docData.receiver_login === setterLogin){
                    objForUpd.receiver_winner_variant = resultLogin;

                    if (docData.sender_winner_variant) {
                        return firestore.collection('matches').doc(matchid)
                                .update({
                                    ...objForUpd,
                                    status: docData.sender_winner_variant === objForUpd.receiver_winner_variant ? MATCH_STATUS_FINISHED : MATCH_STATUS_UNKNOWN
                                })
                    }
                }

                return firestore.collection('matches').doc(matchid)
                        .update({
                            ...objForUpd,
                            status: MATCH_STATUS_DEFINE_WINNER
                        });
            })
};


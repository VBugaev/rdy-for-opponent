import { _createFirstMatchesForLeague } from '../leagues';

describe('leagues test', () => {
    test('create first matches for league - good case', () => {
        const participants = ['vbugaev', 'atyushev', 'vkovalev', 'test1'];

        expect(_createFirstMatchesForLeague(participants, 1, 1)).toEqual([
            {
                user1: 'vbugaev',
                user2: 'atyushev',
                stage: `110`,
                winner: null,
                status: null
            },
            {
                user1: 'vkovalev',
                user2: 'test1',
                stage: `112`,
                winner: null,
                status: null
            }]);
    });
        
});
export const MATCH_STATUS_PENDING = 'pending'; // user sent request
export const MATCH_STATUS_ACCEPTED = 'accepted'; // receiver accepted request
export const MATCH_STATUS_DEFINE_WINNER = 'defineWinner'; // someone proposed winner
export const MATCH_STATUS_FINISHED = 'finished'; // other competitor chose the same winner
export const MATCH_STATUS_UNKNOWN = 'unknown' // competitors chose different winners